function modificar(){
    var id = $("tr.success").find("td").eq(0).html();
    if(id != undefined){
            var vendedor = $("tr.success").find("td").eq(1).html();
            var descripcion = $("tr.success").find("td").eq(4).html();
            document.getElementById('venta_vendedor').value = vendedor;
            document.getElementById('venta_desc').value = descripcion;
            document.getElementById('venta_id').value = id;
            $("#modalModificar").click();
    }
    else
        window.alert("Seleccione una venta");
}

function eliminar(){
    var id = $("tr.success").find("td").eq(0).html();
    if(id != undefined){
            var vendedor = $("tr.success").find("td").eq(1).html();
            var cliente = $("tr.success").find("td").eq(2).html();
            var url="../accion/OperacionesAdmon/eliminar_venta.php";
        if(confirm("Estas Seguro de eliminar la venta de "+vendedor+" a "+cliente)){
            $.ajax({
                url: url,
                type: 'post',
                datatype: 'json',
                data: {id:id},

                success:function(){
                    $("tr.success").remove();
                }
            })
        }
    }
    else
        window.alert("Seleccione una venta");

}
