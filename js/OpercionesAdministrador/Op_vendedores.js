function modificar(){
    var rfc = $("tr.success").find("td").eq(0).html();
    if(rfc != undefined){
        var nombre = $("tr.success").find("td").eq(1).html();
        var apellido_p = $("tr.success").find("td").eq(2).html();
        var am = $("tr.success").find("td").eq(3).html();
        var correo = $("tr.success").find("td").eq(4).html();
        var telefono = $("tr.success").find("td").eq(5).html();
        document.getElementById('v_rfc').value = rfc;
        document.getElementById('v_nombre').value = nombre;
        document.getElementById('v_ap').value = apellido_p;
        document.getElementById('v_am').value = am;
        document.getElementById('v_correo').value = correo;
        document.getElementById('v_telefono').value = telefono;
        $("#modalModificar").click();
        }
    else
        window.alert("Seleccione un vendedor");
}

function eliminar(){
    var rfc = $("tr.success").find("td").eq(0).html();
    var nombre = $("tr.success").find("td").eq(1).html();
    var url="../accion/OperacionesAdmon/eliminar_vendedor.php";
    if(rfc != undefined){
        if(confirm("Estas Seguro de eliminar a "+nombre)){
            $.ajax({
                url: url,
                type: 'post',
                datatype: 'json',
                data: {RFC:rfc},

                success:function(){
                    $("tr.success").remove();
                }
            })
        }
    }
    else
        window.alert("Seleccione un vendedor");

}
