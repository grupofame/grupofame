function aprobar(){
    var id = $("tr.success").find("td").eq(0).html();
    if(id != undefined){
            var vendedor = $("tr.success").find("td").eq(1).html();
            var id_venta = $("tr.success").find("td").eq(2).html();
            var E_Solicitado = $("tr.success").find("td").eq(4).html();
            var url="../accion/OperacionesAdmon/aprobar_p.php";
            if(confirm("Estas Seguro de aprobar la peticion de "+vendedor)){
                $.ajax({
                    url: url,
                    type: 'post',
                    datatype: 'json',
                    data: {id:id,id_venta:id_venta,E_Solicitado:E_Solicitado},
                    success:function(){
                        $("tr.success").remove();
                    }
                })
            }

    }
    else
        window.alert("Seleccione una peticion");
}

function desaprobar(){
    var id = $("tr.success").find("td").eq(0).html();
    if(id != undefined){
            var vendedor = $("tr.success").find("td").eq(1).html();
            var url="../accion/OperacionesAdmon/rechazar_p.php";
        if(confirm("Estas Seguro de rechazar la peticion de "+vendedor)){
                $.ajax({
                    url: url,
                    type: 'post',
                    datatype: 'json',
                    data: {id:id},
                    success:function(){
                        $("tr.success").remove();
                    }
                })
            }

    }
    else
        window.alert("Seleccione una peticion");
        }
