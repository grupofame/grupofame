function modificar(){
    var id = $("tr.success").find("td").eq(0).html();
    if(id != undefined){
        var nombre = $("tr.success").find("td").eq(3).html();
        var apellido_p = $("tr.success").find("td").eq(4).html();
        var am = $("tr.success").find("td").eq(5).html();
        var correo = $("tr.success").find("td").eq(2).html();
        var telefono = $("tr.success").find("td").eq(10).html();
        var estado = $("tr.success").find("td").eq(6).html();
        var ciudad = $("tr.success").find("td").eq(7).html();
        var calle = $("tr.success").find("td").eq(8).html();
        var numero = $("tr.success").find("td").eq(9).html();
        document.getElementById('c_id').value = id;
        document.getElementById('c_nombre').value = nombre;
        document.getElementById('c_ap').value = apellido_p;
        document.getElementById('c_am').value = am;
        document.getElementById('c_correo').value = correo;
        document.getElementById('c_estado').value = estado;
        document.getElementById('c_ciudad').value = ciudad;
        document.getElementById('c_calle').value = calle;
        document.getElementById('c_numero').value = numero;
        document.getElementById('c_telefono').value = telefono;
        $("#modalModificar").click();
        }
    else
        window.alert("Seleccione un cliente");
}

function eliminar(){
    var id = $("tr.success").find("td").eq(0).html();
    var nombre = $("tr.success").find("td").eq(2).html();
    var url="../accion/OperacionesAdmon/eliminar_cliente.php";
    if(id != undefined){
        if(confirm("Estas Seguro de eliminar a "+nombre)){
            $.ajax({
                url: url,
                type: 'post',
                datatype: 'json',
                data: {id:id},

                success:function(){
                    $("tr.success").remove();
                }
            })
        }
    }
    else
        window.alert("Seleccione un cliente");

}
