function modificar(){
    var vin = $("tr.success").find("td").eq(0).html();
    if(vin != undefined){
        var marca = $("tr.success").find("td").eq(1).html();
        var modelo = $("tr.success").find("td").eq(2).html();
        var color = $("tr.success").find("td").eq(3).html();
        var año = $("tr.success").find("td").eq(4).html();
        var precio = $("tr.success").find("td").eq(5).html();
        var Km = $("tr.success").find("td").eq(6).html();
        var transmicion = $("tr.success").find("td").eq(7).html();
        var F_llegada = $("tr.success").find("td").eq(8).html();
        var descuento = $("tr.success").find("td").eq(9).html();
        var puertas = $("tr.success").find("td").eq(10).html();
        var cilindros = $("tr.success").find("td").eq(11).html();
        document.getElementById('a_vin').value = vin;
        document.getElementById('a_marca').value = marca;
        document.getElementById('a_modelo').value = modelo;
        document.getElementById('a_color').value = color;
        document.getElementById('a_año').value = año;
        document.getElementById('a_precio').value = precio;
        document.getElementById('a_km').value = Km;
        document.getElementById('a_transmicion').value = transmicion;
        document.getElementById('a_fllegada').value = F_llegada;
        document.getElementById('a_descuento').value = descuento;
        document.getElementById('a_puertas').value = puertas;
        document.getElementById('a_cilindros').value = cilindros;
        $("#modalModificar").click();
        }
    else
        window.alert("Seleccione un auto");
}

function eliminar(){
    var vin = $("tr.success").find("td").eq(0).html();
    var url="../accion/OperacionesAdmon/eliminar_auto.php";
    if(vin != undefined){
        if(confirm("¿Estas Seguro de eliminar el auto?")){
            $.ajax({
                url: url,
                type: 'post',
                datatype: 'json',
                data: {vin:vin},

                success:function(){
                    $("tr.success").remove();
                }
            })
        }
    }
    else
        window.alert("Seleccione un auto");
}
function modificar_estado(){
    var vin = $("tr.success").find("td").eq(0).html();
    var status = $("tr.success").find("td").eq(14).html();
    if(vin != undefined){
        document.getElementById('a_v_vin').value = vin;
        document.getElementById('a_estado').value = status;
        $("#modalModificar").click();
        }
    else
        window.alert("Seleccione un auto");
}
