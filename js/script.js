$(document).ready(function(){

    var height = $(window).height();
    ajustesIniciales();

    function ajustesIniciales(){
        $("section#contenido").css({"margin-top": height +"px"});
    }

    $(document).scroll(function(){
        var scrollTop = $(this).scrollTop();
        var pixels = scrollTop / 70;

        if(scrollTop < height){
            $("section#contenido-imagen").css({
               "-webkit-filter": "blur(" + pixels +"px)",
               "background-position": "center -" + pixels * 10 + "px"
            });
        }
    });
});
