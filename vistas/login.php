<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../css/styleLogin.css">
</head>
<body>
<div class="form-wrapper">
  <h1>Inicia Sesion</h1>
  <form action="../accion/InicioSesion.php" method="post">
    <div class="form-item">
      <label for="user"></label>
      <input type="text" name="user" required="required" placeholder="Username"></input>
    </div>
    <div class="form-item">
      <label for="password"></label>
      <input type="password" name="password" required="required" placeholder="Password"></input>
    </div>
    <div class="button-panel">
      <input type="submit" class="button" title="Log In" value="Login"></input>
    </div>
  </form>
  <div class="form-footer">
    <p></p>
  </div>
</div>
</body>
</html>
