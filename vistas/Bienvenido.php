<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Clientes</title>
       <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="../css/material.min.css">
    <link rel="stylesheet" href="../css/styleAdmon.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="../js/material.min.js"></script>
</head>
<body>

  <div class="mdl-layout mdl-js-layout">
   <?php include("barraMenu.php");?>

      <main class="mdl-layout__content">
           <h1 id="bienvenido">Bienvenido!</h1>
      </main>
  </div>

    <script src="../js/jquery1.12.4.js"></script>
    <script src="../js/jquery.js" type="text/javascript"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/filtro.js"></script>
    <script src="../js/OpercionesAdministrador/Op_clientes.js"></script>
</html>
