<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Vendedores</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="../css/material.min.css">
    <link rel="stylesheet" href="../css/styleAdmon.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="../js/material.min.js"></script>
</head>
<body>

   <div class="mdl-layout mdl-js-layout">
   <?php include("barraMenu.php");?>

  <main class="mdl-layout__content">
  <div id="Btn-eliminar">
       <button class='mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect' id='Btn-rojo' onclick="eliminar()">
           <i class='material-icons'>remove</i>
       </button><label for="Btn-rojo"><b>Eliminar</b></label>
  </div>
  <div id="Btn-modificar">
       <button class='mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect' id='Btn-azul' onclick="modificar()">
           <span class='glyphicon glyphicon-wrench' aria-hidden='true'><span>
       </button><label for="Btn-azul"><b>Modificar</b></label>
  </div>
    <div id="Btn-nuevo">
      <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab" id="b_nuevo" data-toggle="modal" data-target="#modal_n_vendedor">
          <i class="material-icons">add</i>
      </button><label for="b_nuevo"><b>Vendedor</b></label>
    </div>
    <section id="contenido">
       <div class="table-responsive" id="tabla-vendedores">
        <table class="table table-hover " id="tablaV" >
            <?php
              include("../Accion/Conexion.php");
              $row=$mysqli->query("select * from VendedorVistaAdmon");
              $n=1;
              echo '<thead class="thead-inverse">
                        <tr>
                          <th>RFC</th>
                          <th>Nombre</th>
                          <th>Apellido Paterno</th>
                          <th>Apellido Materno</th>
                          <th>Correo</th>
                          <th>Telefono</th>
                          <th>Ventas</th>
                          <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>';
              while($vendedor=mysqli_fetch_array($row)){
                echo "<tr id='f$n'onclick='activar(id)'>
                          <td>$vendedor[0]</td>
                          <td>$vendedor[1]</td>
                          <td>$vendedor[2]</td>
                          <td>$vendedor[3]</td>
                          <td>$vendedor[4]</td>
                          <td>$vendedor[5]</td>
                          <td>$vendedor[6]</td>";
                          if($vendedor[7]==1)
                          echo "<td>
                                  <button class='mdl-button mdl-js-button mdl-button--fab mdl-button--icon mdl-js-ripple-effect' id='Btn-verde'></button>
                                </td></tr>";
                          else if($vendedor[7]==2){
                              echo "<td>
                                  <button class='mdl-button mdl-js-button mdl-button--fab mdl-button--icon mdl-js-ripple-effect' id='Btn-amarillo'></button>
                                </td></tr>";
                          }
                          else if($vendedor[7]==3){
                              echo "<td>
                                <button class='mdl-button mdl-js-button mdl-button--fab mdl-button--icon mdl-js-ripple-effect' id='Btn-rojo'></button>
                                </td></tr>";
                          }
                  $n++;
              }
                    echo '</tbody>';
            ?>
       </table>

    </div>
  </main>

</div>

<!-- Modal Modificar-->
<?php include("modals/modal_m_vendedor.php"); ?>
<button id="modalModificar" data-toggle="modal" data-target="#modal_m_vendedor"></button>
<!-- Modal Nuevo-->
<?php include("modals/modal_n_vendedor.php"); ?>
</body>

    <script src="../js/jquery1.12.4.js"></script>
    <script src="../js/jquery.js" type="text/javascript"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/filtro.js"></script>
    <script src="../js/OpercionesAdministrador/Op_vendedores.js"></script>
</html>
