<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Autos</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="../css/material.min.css">
    <link rel="stylesheet" href="../css/styleAdmon.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="../js/material.min.js"></script>
</head>
<body>

   <div class="mdl-layout mdl-js-layout">
   <?php include("barraMenu.php");?>

  <main class="mdl-layout__content">
    <div id="Btn-eliminar">
       <button class='mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect' id='Btn-rojo' onclick="eliminar()">
           <i class='material-icons'>remove</i>
       </button><label for="Btn-rojo"><b>Eliminar</b></label>
  </div>
  <div id="Btn-modificar">
       <button class='mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect' id='Btn-azul' onclick="modificar()">
           <span class='glyphicon glyphicon-wrench' aria-hidden='true'><span>
       </button><label for="Btn-azul"><b>Modificar</b></label>
  </div>
    <div id="Btn-nuevo">
      <button class="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab" id="b_nuevo" data-toggle="modal" data-target="#modal_n_vendedor">
          <i class="material-icons">add</i>
      </button><label for="b_nuevo"><b>Vendedor</b></label>
    </div>
    <section id="contenido">
       <div class="table-responsive" id="tabla-vendedores">
        <table class="table table-hover " id="tablaV" >
            <?php
              include("../Accion/Conexion.php");
              $sql="select * from autovitaadmon";
              $row=$mysqli->query("select * from autovitaadmon");
              $n=1;
              echo '<thead class="thead-inverse">
                        <tr>
                          <th>Vin</th>
                          <th>Marca</th>
                          <th>Modelo</th>
                          <th>Color</th>
                          <th>Año</th>
                          <th>Precio</th>
                          <th>KM</th>
                          <th>transmicion</th>
                          <th>Fecha llegada</th>
                          <th>Descuent</th>
                          <th>puertas</th>
                          <th>cilindro</th>
                          <th>oferta</th>
                         <th>Estado</th>
                         <th class="hidden"></th></tr>
                    </thead>
                    <tbody>';
              while($vendedor=mysqli_fetch_array($row)){
                echo "<tr id='f$n'onclick='activar(id)'>
                          <td>$vendedor[0]</td>
                          <td>$vendedor[1]</td>
                          <td>$vendedor[2]</td>
                          <td>$vendedor[3]</td>
                          <td>$vendedor[4]</td>
                          <td>$vendedor[5]</td>
                          <td>$vendedor[6]</td>
                          <td>$vendedor[7]</td>
                          <td>$vendedor[8]</td>
                          <td>$vendedor[9]</td>
                          <td>$vendedor[11]</td>
                          <td>$vendedor[12]</td>
                          <td>$vendedor[13]</td>
                          <td>";
                      if($vendedor[10]==0)
                          echo "No disponible";
                      else
                          echo "Disponible";
                    echo "</td><td class='hidden'>$vendedor[10]</td></tr>";
                  $n++;
              }
                    echo '</tbody>';
            ?>
       </table>
    </div>
  </main>

</div>

<!-- Modals-->
<?php
if($band==1)
include("modals/modal_m_auto.php");
else
include("modals/modal_mv_auto.php");

if($band==1)
echo '<button id="modalModificar" data-toggle="modal" data-target="#modal_m_auto"></button>';
else
echo '<button id="modalModificar" data-toggle="modal" data-target="#modal_mv_auto"></button>';

if($band==1)
include("modals/modal_n_auto.php");
?>
</body>

    <script src="../js/jquery1.12.4.js"></script>
    <script src="../js/jquery.js" type="text/javascript"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/filtro.js"></script>
    <script src="../js/OpercionesAdministrador/Op_auto.js"></script>
</html>
