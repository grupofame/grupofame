<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Peticiones</title>
       <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="../css/material.min.css">
    <link rel="stylesheet" href="../css/styleAdmon.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="../js/material.min.js"></script>
</head>
<body>

   <div class="mdl-layout mdl-js-layout">
   <?php include("barraMenu.php");?>

  <main class="mdl-layout__content">
  <div id="Btn-eliminar">
       <button class='mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect' id='Btn-rojo' onclick="aprobar()">
           <i class='material-icons'>remove</i>
       </button><label for="Btn-rojo"><b>Aprobar</b></label>
  </div>
  <div id="Btn-modificar">
       <button class='mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect' id='Btn-azul' onclick="desaprobar()">
           <span class='glyphicon glyphicon-wrench' aria-hidden='true'><span>
       </button><label for="Btn-azul"><b>Rechazar</b></label>
  </div>
    <section id="contenido">
       <div class="table-responsive" id="tabla-vendedores">
        <table class="table table-hover " id="tablaV" >
            <?php
              include("../Accion/Conexion.php");
              $row=$mysqli->query("select * from PeticionVistaAdmon where status = 1");
              $n=1;
              echo '<thead class="thead-inverse">
                        <tr>
                          <th class="hidden"></th>
                          <th>Vendedor</th>
                          <th>N° Venta</th>
                          <th>Estado Actual</th>
                          <th>Estado solicitado</th>
                        </tr>
                    </thead>
                    <tbody>';
              while($vendedor=mysqli_fetch_array($row)){
                echo "<tr id='f$n'onclick='activar(id)'>
                          <td class='hidden'>$vendedor[0]</td>
                          <td>$vendedor[1]</td>
                          <td>$vendedor[2]</td>
                          <td>$vendedor[3]</td>
                          <td>$vendedor[4]</td></tr>";
                  $n++;}
                    echo '</tbody>';
            ?>
       </table>

    </div>
  </main>

</div>

</body>

    <script src="../js/jquery1.12.4.js"></script>
    <script src="../js/jquery.js" type="text/javascript"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/filtro.js"></script>
    <script src="../js/OpercionesAdministrador/Op_peticiones.js"></script>
</html>
