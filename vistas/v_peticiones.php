<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Clientes</title>
       <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="../css/material.min.css">
    <link rel="stylesheet" href="../css/styleAdmon.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="../js/material.min.js"></script>
</head>
<body>

   <div class="mdl-layout mdl-js-layout">
   <?php include("barraMenu.php");?>

  <main class="mdl-layout__content">

    <section id="contenido">

        <form action="../accion/OperacionesAdmon/SPeticion.php" class="form" style="padding: 15px 15px;" method="post">
           <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label>Venta</label>
                  <select id="v_venta" name="v_venta" class="form-control">
                   <?php
                     $sql="select id_venta, concat(c.nombre,' ',vin,' (',fecha_inicio,')','(',ev.descripcion,')')as ventaa from venta v inner join cliente c on v.id_cliente=c.id_cliente inner join estado_venta ev on ev.id_estado_venta=v.estado_venta_id where RFC_vendedor = '".$_SESSION['u_usuario'][0]."'";
                     $r=$mysqli->query($sql);
                      while( $row = mysqli_fetch_array($r)){
                      echo "<option value='$row[0]'>$row[1]</option>";
                      }
                    ?>
                  </select>
                </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label>Estado deseado</label>
                  <select id="e_deceado" name="venta_desc" class="form-control">
                   <?php
                    $sql="select * from estado_venta";
                    $r=$mysqli->query($sql);
                      while( $row = mysqli_fetch_array($r)){
                      echo "<option value='$row[0]'>$row[1]</option>";
                      }
                    mysqli_close();
                    ?>
                  </select>
                    </div>
            </div></div>
        <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" type="submit">
          Solicitar
        </button>
        </form>

  </main>

</div>

<!-- Modal Modificar-->
<?php include("modals/modal_m_cliente.php"); ?>
<button id="modalModificar" data-toggle="modal" data-target="#modal_m_cliente"></button>
<!-- Modal Nuevo-->
<?php include("modals/modal_n_cliente.php"); ?>
</body>

    <script src="../js/jquery1.12.4.js"></script>
    <script src="../js/jquery.js" type="text/javascript"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/filtro.js"></script>
    <script src="../js/OpercionesAdministrador/Op_clientes.js"></script>
</html>
