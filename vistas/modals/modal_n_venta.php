<div class="modal fade" id="modal_n_venta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <section id="contenido">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Nuevo Venta</h4>
      </div>
      <div class="modal-body">

            <form action="../accion/OperacionesAdmon/nuevo_venta.php" method="post">
             <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                  <label>Vendedor</label>
                  <select id="venta_vendedor" name="venta_vendedor" class="form-control">
                  <option value="">Sin asignar</option>
                   <?php
                    include("../../accion/Conexion.php");
                    $sql="select RFC_vendedor,nombre from vendedor where estado_v_id = 1 and status = 1";
                    $r=$mysqli->query($sql);
                      while( $row = mysqli_fetch_array($r)){
                      echo "<option value='$row[0]'>$row[1]</option>";
                      }
                    ?>
                  </select>
                </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                  <label>Auto</label>
                  <select id="venta_vin" name="venta_vin" class="form-control">
                  <option value="">Sin asignar</option>
                   <?php
                    $sql="select vin,concat(marca,' ',modelo,' ',color) from auto where status = 1";
                    $r=$mysqli->query($sql);
                      while( $row = mysqli_fetch_array($r)){
                      echo "<option value='$row[0]'>$row[1]</option>";
                      }
                    ?>
                  </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                  <label>Cliente</label>
                  <select id="venta_cliente" name="venta_cliente" class="form-control">
                  <option value="">Sin asignar</option>
                   <?php
                    $sql="select id_cliente,nombre from cliente where status = 1";
                    $r=$mysqli->query($sql);
                      while( $row = mysqli_fetch_array($r)){
                      echo "<option value='$row[0]'>$row[1]</option>";
                      }
                    mysqli_close();
                    ?>
                  </select>
                    </div>
                </div>
      </div>
      <div class="modal-footer">
       <div class="form-group">
        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
          Registrar
        </button>
       </div>
      </div>
            </form>
      </section>

  </div>
</div>
</div>
