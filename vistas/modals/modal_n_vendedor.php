<div class="modal fade" id="modal_n_vendedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <section id="contenido">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Nuevo Vendedor</h4>
      </div>
      <div class="modal-body">

            <form action="../accion/OperacionesAdmon/nuevo_vendedor.php" method="post">
             <div class="col-md-6">
                  <div class="form-group">
                      <label for="v_rfc_n" >RFC</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="v_rfc_n" id="v_rfc_n">
                        <label class="mdl-textfield__label" for="v_rfc_n">CDA5462870560</label>
                      </div>
                  </div>
              </div>
             <div class="col-md-6">
                  <div class="form-group">
                      <label for="v_nombre_n" >Nombre</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="v_nombre_n" id="v_nombre_n">
                        <label class="mdl-textfield__label" for="v_nombre_n">Joaquin</label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="v_ap_n" >Apellido Paterno</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="v_ap_n" id="v_ap_n">
                        <label class="mdl-textfield__label" for="v_ap_n">Gonzales</label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="v_am_n" >Apellido Materno</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="v_am_n" id="v_am_n">
                        <label class="mdl-textfield__label" for="v_am_n">Perez</label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="v_telefono_n" >Telefono</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="v_telefono_n" id="v_telefono_n">
                        <label class="mdl-textfield__label" for="v_telefono_n">4432465485</label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="v_correo_n" >Correo Electronico</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="correo" name="v_correo_n" id="v_correo_n">
                        <label class="mdl-textfield__label" for="v_correo_n">JGzl@gmail.com</label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="v_pass_n" >Contraseña</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="password" name="v_pass_n" id="v_pass_n">
                        <label class="mdl-textfield__label" for="v_pass_n"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="v_pass_c_n" >Confirmar contraseña</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="password" name="v_pass_c_n" id="v_pass_c_n">
                        <label class="mdl-textfield__label" for="v_pass_c_n"></label>
                      </div>
                  </div>
              </div>

      </div>
      <div class="modal-footer">
        <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
          Registrar
        </button>
      </div>
            </form>
      </section>

  </div>
</div>
</div>
