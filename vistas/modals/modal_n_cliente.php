<div class="modal fade" id="modal_n_vendedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <section id="contenido">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Nuevo Cliente</h4>
      </div>
      <div class="modal-body">

            <form action="../accion/OperacionesAdmon/nuevo_cliente.php" method="post">
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="c_nombre_n" >Nombre</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_nombre_n" id="c_nombre_n">
                        <label class="mdl-textfield__label" for="c_nombre_n"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="c_ap_n" >Apellido Paterno</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_ap_n" id="c_ap_n">
                        <label class="mdl-textfield__label" for="c_ap_n"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="c_am_n" >Apellido Materno</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_am_n" id="c_am_n">
                        <label class="mdl-textfield__label" for="c_am_n"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-3">
                  <div class="form-group">
                      <label for="c_estado_n" >Estado</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_estado_n" id="c_estado_n">
                        <label class="mdl-textfield__label" for="c_estado_n"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-3">
                  <div class="form-group">
                      <label for="c_ciudad_n" >Ciudad</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_ciudad_n" id="c_ciudad_n">
                        <label class="mdl-textfield__label" for="c_ciudad_n"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="c_calle_n" >Calle</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_calle_n" id="c_calle_n">
                        <label class="mdl-textfield__label" for="c_calle_n"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-2">
                  <div class="form-group">
                      <label for="c_numero_n" >Numero</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="number" name="c_numero_n" id="c_numero_n">
                        <label class="mdl-textfield__label" for="c_numero_n"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-3">
                  <div class="form-group">
                      <label for="c_telefono_n" >Telefono</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="number" name="c_telefono_n" id="c_telefono_n">
                        <label class="mdl-textfield__label" for="c_telefono_n"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-6">
                  <div class="form-group">
                      <label for="c_correo_n" >Correo Electronico</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="correo" name="c_correo_n" id="c_correo_n">
                        <label class="mdl-textfield__label" for="c_correo_n"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-3">
                  <div class="form-group">
                      <label for="c_rfc" >RFC</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_rfc" id="c_rfc">
                        <label class="mdl-textfield__label" for="c_rfc"></label>
                      </div>
                  </div>
              </div>

      </div>
      <div class="modal-footer">
        <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
          Registrar
        </button>
      </div>
            </form>
      </section>

  </div>
</div>
</div>
