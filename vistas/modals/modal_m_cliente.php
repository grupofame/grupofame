<div class="modal fade" id="modal_m_cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <section id="contenido">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modificar cliente</h4>
      </div>
      <div class="modal-body">

            <form action="../accion/OperacionesAdmon/modificar_cliente.php" method="post">
            <input type="hidden" name="c_id" id="c_id">
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="c_nombre" >Nombre</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_nombre" id="c_nombre">
                        <label class="mdl-textfield__label" for="c_nombre"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="c_ap" >Apellido Paterno</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_ap" id="c_ap">
                        <label class="mdl-textfield__label" for="c_ap"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="c_am" >Apellido Materno</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_am" id="c_am">
                        <label class="mdl-textfield__label" for="c_am"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-3">
                  <div class="form-group">
                      <label for="c_estado" >Estado</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_estado" id="c_estado">
                        <label class="mdl-textfield__label" for="c_estado"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-3">
                  <div class="form-group">
                      <label for="c_ciudad" >Ciudad</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_ciudad" id="c_ciudad">
                        <label class="mdl-textfield__label" for="c_ciudad"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-3">
                  <div class="form-group">
                      <label for="c_calle" >Calle</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="c_calle" id="c_calle">
                        <label class="mdl-textfield__label" for="c_calle"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-3">
                  <div class="form-group">
                      <label for="c_numero" >Numero</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="number" name="c_numero" id="c_numero">
                        <label class="mdl-textfield__label" for="c_numero"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="c_telefono" >Telefono</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="number" name="c_telefono" id="c_telefono">
                        <label class="mdl-textfield__label" for="c_telefono"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md8">
                  <div class="form-group">
                      <label for="c_correo" >Correo Electronico</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="correo" name="c_correo" id="c_correo">
                        <label class="mdl-textfield__label" for="c_correo"></label>
                      </div>
                  </div>
              </div>

      </div>
      <div class="modal-footer">
        <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
          Guardar
        </button>
      </div>
            </form>
      </section>

  </div>
</div>
</div>
