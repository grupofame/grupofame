<div class="modal fade" id="modal_m_vendedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <section id="contenido">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modificar vendedor</h4>
      </div>
      <div class="modal-body">

            <form action="../accion/OperacionesAdmon/modificar_vendedor.php" method="post">
            <input type="hidden" name="v_rfc" id="v_rfc">
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="v_nombre" >Nombre</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="v_nombre" id="v_nombre">
                        <label class="mdl-textfield__label" for="v_nombre"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="v_ap" >Apellido Paterno</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="v_ap" id="v_ap">
                        <label class="mdl-textfield__label" for="v_ap"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="v_am" >Apellido Materno</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="v_am" id="v_am">
                        <label class="mdl-textfield__label" for="v_nombre"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="v_telefono" >Telefono</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="v_telefono" id="v_telefono">
                        <label class="mdl-textfield__label" for="v_telefono"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md8">
                  <div class="form-group">
                      <label for="v_correo" >Correo Electronico</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="correo" name="v_correo" id="v_correo">
                        <label class="mdl-textfield__label" for="v_correo"></label>
                      </div>
                  </div>
              </div>

      </div>
      <div class="modal-footer">
        <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
          Guardar
        </button>
      </div>
            </form>
      </section>

  </div>
</div>
</div>
