<div class="modal fade" id="modal_m_auto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <section id="contenido">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modificar auto</h4>
      </div>
      <div class="modal-body">

            <form action="../accion/OperacionesAdmon/modificar_auto.php" method="post">
            <input type="hidden" name="a_vin" id="a_vin">
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="a_marca" >Marca</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="a_marca" id="a_marca">
                        <label class="mdl-textfield__label" for="a_marca"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="a_modelo" >Modelo</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="a_modelo" id="a_modelo">
                        <label class="mdl-textfield__label" for="a_modelo"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="a_color" >color</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="a_color" id="a_color">
                        <label class="mdl-textfield__label" for="a_color"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="a_año" >Año</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="number" name="a_año" id="a_año">
                        <label class="mdl-textfield__label" for="a_año"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="a_precio" >Precio</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="number" name="a_precio" id="a_precio">
                        <label class="mdl-textfield__label" for="a_precio"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="a_km" >Kilometraje</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="number" name="a_km" id="a_km">
                        <label class="mdl-textfield__label" for="a_km"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-6">
                  <div class="form-group">
                      <label for="a_transmicion" >transmiciones</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="text" name="a_transmicion" id="a_transmicion">
                        <label class="mdl-textfield__label" for="a_transmicion"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-6">
                  <div class="form-group">
                      <label for="a_fllegada" >Fecha llegada</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="date" name="a_fllegada" id="a_fllegada">
                        <label class="mdl-textfield__label" for="a_fllegada"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="a_descuento" >descuento</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="number" name="a_descuento" id="a_descuento">
                        <label class="mdl-textfield__label" for="a_descuento"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="a_puertas" >puertas</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="number" name="a_puertas" id="a_puertas">
                        <label class="mdl-textfield__label" for="v_nombre"></label>
                      </div>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                      <label for="a_cilindros" >cilindros</label>
                      <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" type="number" name="a_cilindros" id="a_cilindros">
                        <label class="mdl-textfield__label" for="a_cilindros"></label>
                      </div>
                  </div>
              </div>
      </div>
      <div class="modal-footer">
        <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
          Guardar
        </button>
      </div>
            </form>
      </section>

  </div>
</div>
</div>
