 <div class="modal fade" id="modal_m_venta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <section id="contenido">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cambiar vendedor</h4>
      </div>
      <div class="modal-body">

            <form action="../accion/OperacionesAdmon/modificar_venta.php" method="post">
            <input type="hidden" name="venta_id" id="venta_id">
             <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label>Vendedor</label>
                  <select id="venta_vendedor" name="venta_vendedor" class="form-control">
                  <option value="sa">Sin asignar</option>
                   <?php
                    include("../../accion/Conexion.php");
                    $sql="select nombre from vendedor where and status = 1";
                    $r=$mysqli->query($sql);
                      while( $row = mysqli_fetch_array($r)){
                      echo "<option value='$row[0]'>$row[0]</option>";
                      }
                    ?>
                  </select>
                </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                  <label>Auto</label>
                  <select id="venta_desc" name="venta_desc" class="form-control">
                  <option value="NULL">Sin asignar</option>
                   <?php
                    $sql="select concat(marca,' ',modelo,' ',color) from auto where status = 1";
                    $r=$mysqli->query($sql);
                      while( $row = mysqli_fetch_array($r)){
                      echo "<option value='$row[0]'>$row[0]</option>";
                      }
                    mysqli_close();
                    ?>
                  </select>
                    </div>
                </div>

      </div>
      <div class="modal-footer">
        <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
          Guardar
        </button>
      </div>
            </form>
      </section>

  </div>
</div>
</div>
