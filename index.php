<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GrupoFame</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>    
    <script src="js/menu_sc.js"></script>
    <script src="js/script.js"></script>
</head>
<body>

<nav class="navbar-inverse barraMenu navbar-fixed-top">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>
  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
   <div class="navbar-header">
       <a class="navbar-brand" href="#"><label>Das</label> Welt<label>Auto</label></a>
   </div>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="" class="text">Inicio</a></li>
      <li><a href="" class="text">Galeria de Autos</a></li>
      <li><a href="" class="text">Contacto</a></li>
      <li><a href="" class="text">Como llegar</a></li>
      <li><a href="https://www.facebook.com/dasweltautomorelia/" target="_blank"><img class="icon-social" alt="facebook" src="imagenes/facebook-logo.png"
      title="Siguenos en Facebook"/></a></li>
      <li><a href="https://twitter.com/das_weltauto" target="_blank"><img class="icon-social" alt="twitter" src="imagenes/twitter-logo.png"
      title="Siguenos en Twitter" /></a></li>
      <li><a href="https://www.instagram.com/volkswagen.de/" target="_blank"><img class="icon-social" alt="Instagram" src="imagenes/instagram-logo.png"
  title="Siguenos en Instagram" /></a></li>
    </ul>
  </div><!-- /.navbar-collapse -->

</nav> <!-- Barra de Navegacion -->

    <section id="contenido-imagen"></section>

    <section id="contenido">
      <div class="CatalagoAutos">
        <div class="col-sm-6 col-md-4 col-lg-3">
          <header>
            <div class="menu_bar">
              <a href="#" class="bt-menu">Filtros</a>
            </div>
            <nav class="head_nav">
            
            <div class="filtro">
            <header class="head_der">
              <div>
                 <a href="#" class="accordion-titulo" id="fil_hid">Filtross</a> 
              </div>
            </header>            
            <fieldset>
            <!--<legend>Buscar</legend>-->
              <section>
                <div class="row">
                  <label class="col-xs-4">
                    <span>Palabra Clave</span>
                  </label>
                  <div class="col-xs-8">
                    <label class="input">
                    <input type="text" name="key_word"> 
                    </label>
                  </div>
                </div>
              </section>
              <section>
              <div class="row">
                <div class="col-lg-12">
                  <input id="SearchStartPrice" name="SearchStartPrice" type="hidden" value="119500">
                  <input id="SearchEndPrice" name="SearchEndPrice" type="hidden" value="755500">
                  Kilometraje
                  <span id="kil1">119500</span>
                  -
                  <span id="kil1">755500</span>                  
                  <b>119500 KM</b> 
                  <input id="ex2" type="text" class="span2" value="" data-slider-min="119500" data-slider-max="755500" data-slider-step="5" data-slider-value="[200000,500000]"/> 
                  <b>755500 KM</b>
                  </div>                
              </div>
              </section>
              <section>
              <div class="row">
                <label class="col-xs-4">Precio</label>
                <div class="col-xs-8">
                  <input type="text" name="">
                </div>
              </div>
              </section> 
              <section>
              <div class="row">
                <label class="col-xs-4">Año</label>
                <div class="col-xs-8">
                <div class="row">                  
                  <div class="column-left col-xs-6">
                    <label class="select">
                    <!--<div class="fakedropdown">-DESDE-</div>-->
                    <select name="Srch_year2" id="SearchStartYear" onchange="getInventoryFieldsData('USED','2');">
                      <option value="">-DESDE-</option>
                      <option value="2007">2007</option>
                      <option value="2008">2008</option>
                      <option value="2009">2009</option>
                      <option value="2010">2010</option>
                      <option value="2011">2011</option>
                      <option value="2012">2012</option>
                      <option value="2013">2013</option>
                      <option value="2014">2014</option>
                      <option value="2015">2015</option>
                      <option value="2016">2016</option>
                      <option value="2017">2017</option>
                    </select>
                  </div>
                  <div class="column-left col-xs-6">
                    <label class="select">
                    <!--<div class="fakedropdown">-DESDE-</div>-->
                    <select name="Schh_year2" id="SearchStartYear" onchange="getInventoryFieldsData('USED','2');">
                      <option value="">-Hasta-</option>
                      <option value="2007">2007</option>
                      <option value="2008">2008</option>
                      <option value="2009">2009</option>
                      <option value="2010">2010</option>
                      <option value="2011">2011</option>
                      <option value="2012">2012</option>
                      <option value="2013">2013</option>
                      <option value="2014">2014</option>
                      <option value="2015">2015</option>
                      <option value="2016">2016</option>
                      <option value="2017">2017</option>
                    </select>
                  </div>
                </div>
                </div>
              </div>
              </section>
              <section>
              <div class="row">
                <label class="col-xs-4">Puertas</label>
                <div class="col-xs-8">
                  <label class="select">                    
                    <select name="Srch_puerta" id="SearchStartYear" onchange="getInventoryFieldsData('USED','2');">
                      <option value="">-Puertas-</option>
                      <option value="2">2</option>
                      <option value="4">4</option>
                    </select>
                </div>
              </div>
              </section>
              <section>
              <div class="row">
                <label class="col-xs-4">Cilindros</label>
                <div class="col-xs-8">
                <!-- Poner los valores correctos -->
                  <label class="select">                    
                    <select name="Srch_cilindro" id="SearchStartYear" onchange="getInventoryFieldsData('USED','2');">
                      <option value="">-Cilinros-</option>
                      <option value="2007">No</option>
                      <option value="2008">Sé</option>
                      <option value="2008">de</option>
                      <option value="2008">Cilindraje</option>
                      <option value="2008">:v</option>
                    </select>
                </div>
              </div>
              </section>
              <section>
              <div class="row">
                <label class="col-xs-4">Transmision</label>
                <div class="col-xs-8">
                   <label class="select">                    
                    <select name="Srch_transmision" id="SearchStartYear" onchange="getInventoryFieldsData('USED','2');">
                      <option value="">-Transmision-</option>
                      <option value="Manual">Manual</option>
                      <option value="Automatica">Automatica</option>                      
                    </select>
                </div>
              </div>
              </section>
              <section>
              <div class="row">
                <div class="col-xs-12"> 
                  <div class="accordion-container">                  
                      <a href="#" class="accordion-titulo">Colores
                        <span class="toggle-icon"></span>
                      </a>                  
                    <div class="accordion-content">                
                    <div class="col-xs-2">
                    </div>
                    <div class="col-xs-10">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
   
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    </div>  
                  </div>
                </div>               
              </div>
              </section>
              <section>
              <div class="row">
                <div class="col-xs-12"> 
                  <div class="accordion-container">                  
                      <a href="#" class="accordion-titulo">Marca
                        <span class="toggle-icon"></span>
                      </a>                  
                    <div class="accordion-content">                
                    <div class="col-xs-2">
                    </div>
                    <div class="col-xs-10">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
   
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    </div>  
                  </div>
                </div>               
              </div>
              </section>
            </fieldset>
            </div>
            
        
            </nav>
          </header>
        </div>
        <div class="col-sm-6 col-md-8 col-lg-9" id="hidd">
            <?php 
              include("Accion/Conexion.php");          

              $query = "select vin, year, marca, modelo, precio, kilometraje, descuento from auto where status=1 and oferta=1 ORDER by fecha_llegada";
              $row=$mysqli->query($query);

             while($auto=mysqli_fetch_array($row)){
              $pre = $auto[4]-($auto[4]*($auto[6]/100));
                echo "<div class='col-sm-12 col-md-6 col-lg-4'>
                        <div class='ficha' title='INFORMACION Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                                        Ut diam metus, venenatis ullamcorper, consequat sit amet, volutpat at, nulla.
                                        Nulla sollicitudin metus Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                        Ut diam metus, venenatis ullamcorper, consequat sit amet, volutpat at, nulla. Nulla sollicitudin metus.'>
                          <div class='auto_mini' id='capa' style='background: url(img/auto.jpg) no-repeat center center;'>      
                            
                          </div>
                          <div class='auto_inf'>
                            $auto[1] • $auto[2] $auto[3]
                          </div>          
                          <div class='auto_dat'>
                            <div class='dat_pre'>
                              <div class='dat_cont'>
                                ¡Oferta! de &#36;<strike>$auto[4]</strike> a &#36;$pre
                              </div>      
                            </div>
                            <div class='dat_kil'>
                              <div class='dat_cont'>
                                Kilometraje $auto[5]
                              </div>      
                            </div>      
                          </div>  
                            <div class='auto_vent'> 
                              <div class='vent_inf'>
                                <form action='auto.php' method='post'>
                                <input type='hidden' name='key' value=$auto[0] />
                                <input type='submit' value='Más información' />
                                </form>                                
                                <a href='auto.html'>Me interesa</a>
                              </div>  
                            </div>                
                        </div>
                      </div>";
              }

              $query = "select vin, year, marca, modelo, precio, kilometraje from auto where status=1 and oferta=0 ORDER by fecha_llegada";
              $row=$mysqli->query($query);

             while($auto=mysqli_fetch_array($row)){
              
                echo "<div class='col-sm-12 col-md-6 col-lg-4'>
                        <div class='ficha' title='INFORMACION Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                                        Ut diam metus, venenatis ullamcorper, consequat sit amet, volutpat at, nulla.
                                        Nulla sollicitudin metus Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                        Ut diam metus, venenatis ullamcorper, consequat sit amet, volutpat at, nulla. Nulla sollicitudin metus.'>
                          <div class='auto_mini' id='capa' style='background: url(img/auto.jpg) no-repeat center center;'>      
                            
                          </div>
                          <div class='auto_inf'>
                            $auto[1] • $auto[2] $auto[3]
                          </div>          
                          <div class='auto_dat'>
                            <div class='dat_pre'>
                              <div class='dat_cont'>
                                 &#36; $auto[4]
                              </div>      
                            </div>
                            <div class='dat_kil'>
                              <div class='dat_cont'>
                                Kilometraje $auto[5]
                              </div>      
                            </div>      
                          </div>  
                            <div class='auto_vent'> 
                              <div class='vent_inf'>
                                <A HREF='auto.html'>Más información</A>
                                <a href='auto.html'>Me interesa</a>
                              </div>  
                            </div>                
                        </div>
                      </div>";
              }
            ?>          
            </div>
      </div>
    </section>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
